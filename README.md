# Java based web application to calculate and pay the salary of employees 


# Technology Use Here:

✓ Spring boot, Spring JPA
✓ Rest Controller
✓ MySQL
✓ Maven
✓ Thymeleaf
✓ jQuery
✓ Bootstrap


## How to run this application


- git clone https://chabedalam11@bitbucket.org/chabedalam11/assignment-app.git
- cd assignment-app
- open assignment-app\src\main\resources\application.properties file
- change spring.datasource.password property
- mvn spring-boot:run
- browse url http://localhost:8090


## Preview Appliaction

- [Preview Application From Test Server](http://103.218.26.131:8090/)