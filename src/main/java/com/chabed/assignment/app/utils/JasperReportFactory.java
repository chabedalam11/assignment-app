package com.chabed.assignment.app.utils;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;

@Component
public class JasperReportFactory {
	private static final Logger log = LoggerFactory.getLogger(JasperReportFactory.class);
	private static final String TAG = "JasperReportFactory :: {} ";

//	private static final String REPORT_DIR = "\\src\\main\\resources\\static\\report\\";

	public void openReport(Connection conn, String fileName, HashMap<String, Object> hmParams,
			HttpServletResponse response) {
		JasperReport jasperReport = getCompiledFile(fileName);
		try {
			generateReportPDF(response, hmParams, jasperReport, conn);
		} catch (Exception e) {
			e.printStackTrace();
			log.info(TAG, e.getMessage());
		}
	}

	/* for compile */
	 /* public JasperReport getCompiledFile(String fileName){ 
		  try { 
			  File  reportFile = new File(System.getProperty("user.dir") + REPORT_DIR +fileName+".jasper"); 
			  if (!reportFile.exists()) {
				  JasperCompileManager.compileReportToFile( 
						  System.getProperty("user.dir") + REPORT_DIR +fileName+".jrxml", 
						  System.getProperty("user.dir") + REPORT_DIR  +fileName+".jasper" 
						  ); 
			  } 
			  JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(reportFile.getPath());
			  return jasperReport;
		  } catch (Exception e) {
			  e.printStackTrace(); log.info(TAG,e.getMessage());
			  return null; 
		  } 
	  }*/
	 

	/* for server */
	public JasperReport getCompiledFile(String fileName) {
		try {
			Resource resource = new ClassPathResource(AppConstants.REPORT_DIR + fileName + ".jasper");
			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(resource.getInputStream());
			return jasperReport;
		} catch (Exception e) {
			e.printStackTrace();
			log.info(TAG, e.getMessage());
			return null;
		}
	}

	private void generateReportPDF(HttpServletResponse resp, Map<String, Object> parameters, JasperReport jasperReport,
			Connection conn) throws JRException, NamingException, SQLException, IOException {
		byte[] bytes = null;
		bytes = JasperRunManager.runReportToPdf(jasperReport, parameters, conn);
		resp.reset();
		resp.resetBuffer();
		resp.setContentType("application/pdf");
		resp.setContentLength(bytes.length);
		ServletOutputStream ouputStream = resp.getOutputStream();
		ouputStream.write(bytes, 0, bytes.length);
		ouputStream.flush();
		conn.close();
	}
}
