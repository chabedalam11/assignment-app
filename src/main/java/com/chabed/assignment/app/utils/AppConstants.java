package com.chabed.assignment.app.utils;

public interface AppConstants {
	
	/*report properties*/
	String REPORT_DIR = "static\\report\\";
	
	/* server message */
	String MSG_SAVE_DISPLAY = "info save successfull";
	String MSG_UPDATE_DISPLAY = "info update successfull";
	String MSG_DELETE_DISPLAY = "info delete successfull";
	String MSG_TRANSACTION_ERROR = "unable to complete transaction";
	
	
}
