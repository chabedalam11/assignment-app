package com.chabed.assignment.app.utils;


import org.springframework.stereotype.Component;

import com.chabed.assignment.app.model.AppJsonReturn;




@Component
public class AppUtils {
	public void setTransactionError(AppJsonReturn jsonReturn) {
		jsonReturn.setSuccess(false);
		jsonReturn.setMessage(AppConstants.MSG_TRANSACTION_ERROR);
	}
	
	
}
