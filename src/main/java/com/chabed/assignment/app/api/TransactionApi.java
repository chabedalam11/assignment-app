package com.chabed.assignment.app.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chabed.assignment.app.entity.ComAccount;
import com.chabed.assignment.app.entity.Transaction;
import com.chabed.assignment.app.model.AppJsonReturn;
import com.chabed.assignment.app.service.TransactionService;
import com.chabed.assignment.app.utils.AppConstants;
import com.chabed.assignment.app.utils.AppUtils;


@RestController
@RequestMapping("/api/transaction")
public class TransactionApi {

	private static final Logger log = LoggerFactory.getLogger(TransactionApi.class);
	private static final String TAG = "TransactionApi :: {} ";
	
	@Autowired
	AppUtils appUtils;
	
    @Autowired
    private TransactionService transactionService;
        
    @GetMapping
    public List<Transaction> getAllTransaction() {
    	log.info(TAG, "get Transaction");
        return transactionService.getAllTransaction();
    }
    
//  @GetMapping("/{id}")
//  public ResponseEntity<User> findUserById(@PathVariable(value = "id") long id) {
//     // Implement
//  }
    
    @GetMapping("/{comId}")
    public List<Transaction> getByComId(@PathVariable(value = "comId") int comId) {
    	log.info(TAG, "get Transaction by com Id");
    	return transactionService.findByComAccount(new ComAccount(comId));
    }

    @PostMapping
    public AppJsonReturn saveTransaction(@RequestBody Transaction obj) {
    	AppJsonReturn jsonReturn = new AppJsonReturn();
		log.info(TAG, "save Transaction");
		if (transactionService.saveTransaction(obj) != null) {
			jsonReturn.setSuccess(true);
			jsonReturn.setMessage(AppConstants.MSG_SAVE_DISPLAY);
		} else {
			appUtils.setTransactionError(jsonReturn);
		}
		return jsonReturn;
    }
    
    @PutMapping
    public AppJsonReturn updateTransaction(@RequestBody Transaction obj) {
    	AppJsonReturn jsonReturn = new AppJsonReturn();
		log.info(TAG, "update Transaction");
		if (transactionService.updateTransaction(obj) != null ) {
			jsonReturn.setSuccess(true);
			jsonReturn.setMessage(AppConstants.MSG_UPDATE_DISPLAY);
		} else {
			appUtils.setTransactionError(jsonReturn);
		}
		return jsonReturn;
    }
    
    @DeleteMapping
    public AppJsonReturn deleteTransaction(@RequestBody Transaction obj) {
    	AppJsonReturn jsonReturn = new AppJsonReturn();
		log.info(TAG, "delete Transaction");
		if (transactionService.deleteTransaction(obj)) {
			jsonReturn.setSuccess(true);
			jsonReturn.setMessage(AppConstants.MSG_DELETE_DISPLAY);
		} else {
			appUtils.setTransactionError(jsonReturn);
		}
		return jsonReturn;
    }
}
