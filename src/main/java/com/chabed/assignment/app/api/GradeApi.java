package com.chabed.assignment.app.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chabed.assignment.app.entity.Grade;
import com.chabed.assignment.app.model.AppJsonReturn;
import com.chabed.assignment.app.service.GradeService;
import com.chabed.assignment.app.utils.AppConstants;
import com.chabed.assignment.app.utils.AppUtils;


@RestController
@RequestMapping("/api/grade")
public class GradeApi {

	private static final Logger log = LoggerFactory.getLogger(GradeApi.class);
	private static final String TAG = "GradeApi :: {} ";
	
	@Autowired
	AppUtils appUtils;
	
    @Autowired
    private GradeService gradeService;
    
    private final int  ADD_AMOUNT = 5000;
    private final int  HOUSE_RENT = 20;
    private final int  MEDICAL_ALLOWANCE = 15;
        
    
    
    @GetMapping
    public List<Grade> getAllgrade() {
    	log.info(TAG, "get grade");
        return gradeService.getAllGrade();
    }

    @PostMapping
    public AppJsonReturn savegrade(@RequestBody Grade obj) {
    	AppJsonReturn jsonReturn = new AppJsonReturn();
		log.info(TAG, "save grade");
		
		Grade lastSavedGrade = new Grade();
		for(int i = 6; i >= 1; i--) {
			Grade grade = new Grade();
			double basicSalary = i == 6 ? obj.getBasic() : lastSavedGrade.getBasic()+ADD_AMOUNT;
			int maxPost = i==1 || i == 2 ? 1 : 2;
			
			grade.setMaxPost(maxPost);
			grade.setBasic(basicSalary);
			grade.setName("Grade/Rank - "+i);
			
			
			double houseRent = (basicSalary / 100) * HOUSE_RENT;
			double medical = (basicSalary / 100) * MEDICAL_ALLOWANCE;
			double salary = basicSalary + houseRent + medical;
			
			grade.setHouseRent(houseRent);
			grade.setMedical(medical);
			grade.setSalary(salary);
			
			lastSavedGrade = gradeService.saveGrade(grade);
			
			if (lastSavedGrade != null) {
				jsonReturn.setSuccess(true);
				jsonReturn.setMessage(AppConstants.MSG_SAVE_DISPLAY);
			} else {
				appUtils.setTransactionError(jsonReturn);
			}
			
		}
		return jsonReturn;
    }
    
    @PutMapping
    public AppJsonReturn updategrade(@RequestBody Grade obj) {
    	AppJsonReturn jsonReturn = new AppJsonReturn();
		log.info(TAG, "update grade");
		
		List<Grade> grades = gradeService.getAllGrade();
		Grade lastSavedGrade = new Grade();
		if(grades.get(0).getId() == obj.getId()) { // for lowest grade
			for(int i = 0; i< grades.size(); i++) {
				Grade dbGrade = grades.get(i);
				double basicSalary = i == 0 ? obj.getBasic() : lastSavedGrade.getBasic()+ADD_AMOUNT;

				dbGrade.setBasic(basicSalary);
				
				
				double houseRent = (basicSalary / 100) * HOUSE_RENT;
				double medical = (basicSalary / 100) * MEDICAL_ALLOWANCE;
				double salary = basicSalary + houseRent + medical;
				
				dbGrade.setHouseRent(houseRent);
				dbGrade.setMedical(medical);
				dbGrade.setSalary(salary);
				
				lastSavedGrade = gradeService.updateGrade(dbGrade);
				
				if (lastSavedGrade != null ) {
					jsonReturn.setSuccess(true);
					jsonReturn.setMessage(AppConstants.MSG_UPDATE_DISPLAY);
				} else {
					appUtils.setTransactionError(jsonReturn);
				}
				
			}
		}else { // for other grade except lowest
			Grade bdObj = gradeService.fintById(obj.getId());
			if (gradeService.updateGrade(bdObj) != null ) {
				jsonReturn.setSuccess(true);
				jsonReturn.setMessage(AppConstants.MSG_UPDATE_DISPLAY);
			} else {
				appUtils.setTransactionError(jsonReturn);
			}
		}
		
		return jsonReturn;
    }
    
    @DeleteMapping
    public AppJsonReturn deletegrade(@RequestBody Grade obj) {
    	AppJsonReturn jsonReturn = new AppJsonReturn();
		log.info(TAG, "delete grade");
		if (gradeService.deleteGrade(obj)) {
			jsonReturn.setSuccess(true);
			jsonReturn.setMessage(AppConstants.MSG_DELETE_DISPLAY);
		} else {
			appUtils.setTransactionError(jsonReturn);
		}
		return jsonReturn;
    }
}
