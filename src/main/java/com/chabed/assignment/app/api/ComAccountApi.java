package com.chabed.assignment.app.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chabed.assignment.app.entity.ComAccount;
import com.chabed.assignment.app.entity.Transaction;
import com.chabed.assignment.app.model.AppJsonReturn;
import com.chabed.assignment.app.model.ComAccountModel;
import com.chabed.assignment.app.service.ComAccountService;
import com.chabed.assignment.app.service.TransactionService;
import com.chabed.assignment.app.utils.AppConstants;
import com.chabed.assignment.app.utils.AppUtils;


@RestController
@RequestMapping("/api/comAccount")
public class ComAccountApi {

	private static final Logger log = LoggerFactory.getLogger(ComAccountApi.class);
	private static final String TAG = "ComAccountApi :: {} ";
	
	@Autowired
	AppUtils appUtils;
	
    @Autowired
    private ComAccountService comAccountService;
    
    @Autowired
    private TransactionService transactionService;
        
    @GetMapping
    public List<ComAccount> getAllComAccount() {
    	log.info(TAG, "get ComAccount");
        return comAccountService.getAllComAccount();
    }

    @PostMapping
    public AppJsonReturn saveComAccount(@RequestBody ComAccountModel obj) {
    	AppJsonReturn jsonReturn = new AppJsonReturn();
		log.info(TAG, "save ComAccount");
		
		ComAccount comAccount = comAccountService.saveComAccount(obj.getComAccount());
		
		Transaction transaction = obj.getTransaction();
		transaction.setComAccount(comAccount);
		
		if (comAccount != null && transactionService.saveTransaction(transaction) != null) {
			jsonReturn.setSuccess(true);
			jsonReturn.setMessage(AppConstants.MSG_SAVE_DISPLAY);
		} else {
			appUtils.setTransactionError(jsonReturn);
		}
		return jsonReturn;
    }
    
    @PutMapping
    public AppJsonReturn updateComAccount(@RequestBody ComAccount obj) {
    	AppJsonReturn jsonReturn = new AppJsonReturn();
		log.info(TAG, "update ComAccount");
		if (comAccountService.updateComAccount(obj) != null ) {
			jsonReturn.setSuccess(true);
			jsonReturn.setMessage(AppConstants.MSG_UPDATE_DISPLAY);
		} else {
			appUtils.setTransactionError(jsonReturn);
		}
		return jsonReturn;
    }
    
    @DeleteMapping
    public AppJsonReturn deleteComAccount(@RequestBody ComAccount obj) {
    	AppJsonReturn jsonReturn = new AppJsonReturn();
		log.info(TAG, "delete ComAccount");
		if (comAccountService.deleteComAccount(obj)) {
			jsonReturn.setSuccess(true);
			jsonReturn.setMessage(AppConstants.MSG_DELETE_DISPLAY);
		} else {
			appUtils.setTransactionError(jsonReturn);
		}
		return jsonReturn;
    }
}
