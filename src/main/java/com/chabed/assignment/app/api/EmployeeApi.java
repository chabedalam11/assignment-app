package com.chabed.assignment.app.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chabed.assignment.app.entity.Employee;
import com.chabed.assignment.app.model.AppJsonReturn;
import com.chabed.assignment.app.service.EmployeeService;
import com.chabed.assignment.app.utils.AppConstants;
import com.chabed.assignment.app.utils.AppUtils;


@RestController
@RequestMapping("/api/employee")
public class EmployeeApi {

	private static final Logger log = LoggerFactory.getLogger(EmployeeApi.class);
	private static final String TAG = "EmployeeApi :: {} ";
	
	@Autowired
	AppUtils appUtils;
	
    @Autowired
    private EmployeeService employeeService;
        
    @GetMapping
    public List<Employee> getAllEmployee() {
    	log.info(TAG, "get employee");
        return employeeService.getAllEmployee();
    }

//    @GetMapping("/{id}")
//    public ResponseEntity<User> findUserById(@PathVariable(value = "id") long id) {
//       // Implement
//    }

    @PostMapping
    public AppJsonReturn saveEmployee(@RequestBody Employee obj) {
    	AppJsonReturn jsonReturn = new AppJsonReturn();
		log.info(TAG, "save employee");
		if (employeeService.saveEmployee(obj) != null) {
			jsonReturn.setSuccess(true);
			jsonReturn.setMessage(AppConstants.MSG_SAVE_DISPLAY);
		} else {
			appUtils.setTransactionError(jsonReturn);
		}
		return jsonReturn;
    }
    
    @PutMapping
    public AppJsonReturn updateEmployee(@RequestBody Employee obj) {
    	AppJsonReturn jsonReturn = new AppJsonReturn();
		log.info(TAG, "update employee");
		if (employeeService.updateEmployee(obj) != null ) {
			jsonReturn.setSuccess(true);
			jsonReturn.setMessage(AppConstants.MSG_UPDATE_DISPLAY);
		} else {
			appUtils.setTransactionError(jsonReturn);
		}
		return jsonReturn;
    }
    
    @DeleteMapping
    public AppJsonReturn deleteEmployee(@RequestBody Employee obj) {
    	AppJsonReturn jsonReturn = new AppJsonReturn();
		log.info(TAG, "delete employee");
		if (employeeService.deleteEmployee(obj)) {
			jsonReturn.setSuccess(true);
			jsonReturn.setMessage(AppConstants.MSG_DELETE_DISPLAY);
		} else {
			appUtils.setTransactionError(jsonReturn);
		}
		return jsonReturn;
    }
}
