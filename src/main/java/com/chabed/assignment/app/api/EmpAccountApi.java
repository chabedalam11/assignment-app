package com.chabed.assignment.app.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.chabed.assignment.app.entity.EmpAccount;
import com.chabed.assignment.app.model.AppJsonReturn;
import com.chabed.assignment.app.service.EmpAccountService;
import com.chabed.assignment.app.utils.AppConstants;
import com.chabed.assignment.app.utils.AppUtils;


@RestController
@RequestMapping("/api/empAccount")
public class EmpAccountApi {

	private static final Logger log = LoggerFactory.getLogger(EmpAccountApi.class);
	private static final String TAG = "EmpAccountApi :: {} ";
	
	@Autowired
	AppUtils appUtils;
	
    @Autowired
    private EmpAccountService EmpAccountService;
        
    @GetMapping
    public List<EmpAccount> getAllEmpAccount() {
    	log.info(TAG, "get EmpAccount");
        return EmpAccountService.getAllEmpAccount();
    }

    @PostMapping
    public AppJsonReturn saveEmpAccount(@RequestBody EmpAccount obj) {
    	AppJsonReturn jsonReturn = new AppJsonReturn();
		log.info(TAG, "save EmpAccount");
		if (EmpAccountService.saveEmpAccount(obj) != null) {
			jsonReturn.setSuccess(true);
			jsonReturn.setMessage(AppConstants.MSG_SAVE_DISPLAY);
		} else {
			appUtils.setTransactionError(jsonReturn);
		}
		return jsonReturn;
    }
    
    @PutMapping
    public AppJsonReturn updateEmpAccount(@RequestBody EmpAccount obj) {
    	AppJsonReturn jsonReturn = new AppJsonReturn();
		log.info(TAG, "update EmpAccount");
		if (EmpAccountService.updateEmpAccount(obj) != null ) {
			jsonReturn.setSuccess(true);
			jsonReturn.setMessage(AppConstants.MSG_UPDATE_DISPLAY);
		} else {
			appUtils.setTransactionError(jsonReturn);
		}
		return jsonReturn;
    }
    
    @DeleteMapping
    public AppJsonReturn deleteEmpAccount(@RequestBody EmpAccount obj) {
    	AppJsonReturn jsonReturn = new AppJsonReturn();
		log.info(TAG, "delete EmpAccount");
		if (EmpAccountService.deleteEmpAccount(obj)) {
			jsonReturn.setSuccess(true);
			jsonReturn.setMessage(AppConstants.MSG_DELETE_DISPLAY);
		} else {
			appUtils.setTransactionError(jsonReturn);
		}
		return jsonReturn;
    }
}
