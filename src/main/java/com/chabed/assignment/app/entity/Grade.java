package com.chabed.assignment.app.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "GRADE")
public class Grade {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ID")
	private int id;
	
	@Column(name = "NAME", length=50)
	private String name;
	
	@Column(name = "MAX_POST")
	private int maxPost;
	
	@Column(name = "BASIC")
	private double basic;
	
	@Column(name = "HOUSE_RENT")
	private double houseRent;
	
	@Column(name = "MEDICAL")
	private double medical;
	
	@Column(name = "SALARY")
	private double salary;
	
	
	public Grade() {
        super();
    }


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getMaxPost() {
		return maxPost;
	}


	public void setMaxPost(int maxPost) {
		this.maxPost = maxPost;
	}


	public double getBasic() {
		return basic;
	}


	public void setBasic(double basic) {
		this.basic = basic;
	}


	public double getHouseRent() {
		return houseRent;
	}


	public void setHouseRent(double houseRent) {
		this.houseRent = houseRent;
	}


	public double getMedical() {
		return medical;
	}


	public void setMedical(double medical) {
		this.medical = medical;
	}


	public double getSalary() {
		return salary;
	}


	public void setSalary(double salary) {
		this.salary = salary;
	}

	
}
