package com.chabed.assignment.app.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "TRANSACTION")
public class Transaction {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ID")
	private int id;
	
	@Column(name = "DES", length=100)
	private String des;
	
	@Column(name = "CR_AMOUNT")
	private double crAmount;
	
	@Column(name = "DR_AMOUNT")
	private double drAmount;
	
	@OneToOne
    @JoinColumn(name="COM_ACC_ID", referencedColumnName="ID")
    private ComAccount comAccount;
	
	@OneToOne
    @JoinColumn(name="EMP_ACC_ID", referencedColumnName="ID")
    private EmpAccount empAccount;
	
	public Transaction() {
        super();
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public double getCrAmount() {
		return crAmount;
	}

	public void setCrAmount(double crAmount) {
		this.crAmount = crAmount;
	}

	public double getDrAmount() {
		return drAmount;
	}

	public void setDrAmount(double drAmount) {
		this.drAmount = drAmount;
	}

	public ComAccount getComAccount() {
		return comAccount;
	}

	public void setComAccount(ComAccount comAccount) {
		this.comAccount = comAccount;
	}

	public EmpAccount getEmpAccount() {
		return empAccount;
	}

	public void setEmpAccount(EmpAccount empAccount) {
		this.empAccount = empAccount;
	}
	
	
	
}
