package com.chabed.assignment.app.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "EMP_ACCOUNT")
public class EmpAccount {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ID")
	private int id;
	
	@Column(name = "TYPE", length=20)
	private String type;
	
	@Column(name = "ACC_NAME", length=50)
	private String accName;
	
	@Column(name = "ACC_NUMBER", length=50)
	private String accNumber;
	
	@Column(name = "BACK_NAME", length=100)
	private String bankName;
	
	@Column(name = "BRANCH_NAME", length=100)
	private String branchName;
	
	@Column(name = "BALANCE")
	private double balance;
	
	@OneToOne
    @JoinColumn(name="EMP_ID", referencedColumnName="ID")
    private Employee employee;
	
	
	public EmpAccount() {
        super();
    }


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getAccName() {
		return accName;
	}


	public void setAccName(String accName) {
		this.accName = accName;
	}


	public String getAccNumber() {
		return accNumber;
	}


	public void setAccNumber(String accNumber) {
		this.accNumber = accNumber;
	}


	public String getBankName() {
		return bankName;
	}


	public void setBankName(String bankName) {
		this.bankName = bankName;
	}


	public String getBranchName() {
		return branchName;
	}


	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}


	public double getBalance() {
		return balance;
	}


	public void setBalance(double balance) {
		this.balance = balance;
	}


	public Employee getEmployee() {
		return employee;
	}


	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

}
