package com.chabed.assignment.app.authentication;

import java.util.Collections;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;


@Component
public class CustomAuthenticationProvider implements AuthenticationProvider{
	
	
	
	 @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String userName = authentication.getName();
        String password = authentication.getCredentials().toString();
        
        
        if(userName.equals("admin") && password.equals("admin")) {
        	 return new UsernamePasswordAuthenticationToken
                     (userName, password, Collections.emptyList());
		}else {
			throw new BadCredentialsException("External system authentication failed");
		}
    }
 
 
    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }

	
}
