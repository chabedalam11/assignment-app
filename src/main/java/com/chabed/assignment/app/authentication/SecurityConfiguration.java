package com.chabed.assignment.app.authentication;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private CustomAuthenticationProvider authenticationProvider;

	@Override
	public void configure(AuthenticationManagerBuilder auth)
	          throws Exception {
	      auth.authenticationProvider(authenticationProvider);
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
	      http.authorizeRequests()
	        .antMatchers("/").permitAll()
			.antMatchers("/login").permitAll()
			.antMatchers("/api/**").permitAll()
	        .antMatchers("/admin/**")
	        .authenticated()
	        .and()
	        .csrf()
	        .disable()
	        .formLogin()
	        .loginPage("/login").failureUrl("/login?error=true")
	  		.defaultSuccessUrl("/admin/home")
	  		.usernameParameter("email")
	  		.passwordParameter("password")
	  		.and().logout()
	  		.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
	  		.logoutSuccessUrl("/login").and().exceptionHandling()
	  		.accessDeniedPage("/access-denied");
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
	    web
	       .ignoring()
	       .antMatchers("/resources/**", "/static/**", "/css/**","/fonts/**", "/img/**","/js/**","/vendors/**","/app/**","/report/**");
	}
	
}