package com.chabed.assignment.app.report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.chabed.assignment.app.utils.JasperReportFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.sql.DataSource;

@Controller
public class SalarySheetReport {
	private static final Logger log = LoggerFactory.getLogger(SalarySheetReport.class);
	private static final String TAG ="SalarySheetReport :: {} ";
	
	@Autowired
	JasperReportFactory jasperFactory;
	
	@Autowired
	DataSource datasource;
	
	
	@RequestMapping("/admin/reportSalarySheet")
	public String reportSalarySheet(HttpServletResponse response) {
		log.info(TAG,"call for Report Salary Sheet : ");
		//Parameters as Map to be passed to Jasper
		HashMap<String,Object> hmParams=new HashMap<String,Object>();
		
		Connection conn = null;
		try {
			conn = datasource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		/* call report for response */
		jasperFactory.openReport(conn,"salarySheet",hmParams, response);
		return null;
	}
	
	
	
}
