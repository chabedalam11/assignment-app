package com.chabed.assignment.app.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.chabed.assignment.app.entity.ComAccount;


@Repository("ComAccountRepository")
public interface ComAccountRepository extends JpaRepository<ComAccount, Integer> {
	ComAccount findById(int id);
}
