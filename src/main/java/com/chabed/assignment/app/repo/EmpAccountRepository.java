package com.chabed.assignment.app.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.chabed.assignment.app.entity.EmpAccount;


@Repository("EmpAccountRepository")
public interface EmpAccountRepository extends JpaRepository<EmpAccount, Integer> {
	EmpAccount findById(int id);
}
