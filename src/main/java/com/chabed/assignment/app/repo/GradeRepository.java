package com.chabed.assignment.app.repo;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.chabed.assignment.app.entity.Grade;


@Repository("GradeRepository")
public interface GradeRepository extends JpaRepository<Grade, Integer> {
	Grade findById(int id);
	List<Grade> findAllByOrderByIdAsc();
}
