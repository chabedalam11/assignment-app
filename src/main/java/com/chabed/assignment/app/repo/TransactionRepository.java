package com.chabed.assignment.app.repo;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.chabed.assignment.app.entity.ComAccount;
import com.chabed.assignment.app.entity.Transaction;


@Repository("TransactionRepository")
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {
	Transaction findById(int id);
	
	List<Transaction> findByComAccount(ComAccount comAccount);
}
