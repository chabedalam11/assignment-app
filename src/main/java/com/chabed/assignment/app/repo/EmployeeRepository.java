package com.chabed.assignment.app.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.chabed.assignment.app.entity.Employee;


@Repository("EmployeeRepository")
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
	Employee findById(int id);
}
