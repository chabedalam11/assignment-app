package com.chabed.assignment.app.service;



import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chabed.assignment.app.entity.EmpAccount;
import com.chabed.assignment.app.repo.EmpAccountRepository;


@Service("EmpAccountService")
public class EmpAccountServiceImpl implements EmpAccountService{
	private static final Logger log = LoggerFactory.getLogger(EmpAccountServiceImpl.class);
	private static final String TAG ="EmpAccountServiceImpl :: {} ";

	@Autowired
	private EmpAccountRepository empAccountRepository;
	

	@Override
	public List<EmpAccount> getAllEmpAccount() {
		return empAccountRepository.findAll();
	}
	
	@Override
	public EmpAccount saveEmpAccount(EmpAccount obj) {
		return empAccountRepository.save(obj);
	}

	@Override
	public EmpAccount updateEmpAccount(EmpAccount obj) {
		return empAccountRepository.save(obj);
	}

	@Override
	public boolean deleteEmpAccount (EmpAccount obj) {
		try {
			empAccountRepository.delete(obj);
			return true;
		}catch(Exception ex) {
			log.error(TAG, ex.getMessage());
			return false;
		}
	}

	
}
