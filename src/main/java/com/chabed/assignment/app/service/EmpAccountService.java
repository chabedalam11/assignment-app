package com.chabed.assignment.app.service;


import java.util.List;

import com.chabed.assignment.app.entity.EmpAccount;


public interface EmpAccountService {
	public List<EmpAccount> getAllEmpAccount();
	public EmpAccount saveEmpAccount(EmpAccount obj);
	public EmpAccount updateEmpAccount(EmpAccount obj);
	public boolean deleteEmpAccount(EmpAccount obj);
}
