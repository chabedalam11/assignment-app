package com.chabed.assignment.app.service;


import java.util.List;

import com.chabed.assignment.app.entity.ComAccount;


public interface ComAccountService {
	public List<ComAccount> getAllComAccount();
	public ComAccount saveComAccount(ComAccount obj);
	public ComAccount updateComAccount(ComAccount obj);
	public boolean deleteComAccount(ComAccount obj);
}
