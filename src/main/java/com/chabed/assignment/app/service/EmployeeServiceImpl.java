package com.chabed.assignment.app.service;



import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chabed.assignment.app.entity.Employee;
import com.chabed.assignment.app.repo.EmployeeRepository;


@Service("EmployeeService")
public class EmployeeServiceImpl implements EmployeeService{
	private static final Logger log = LoggerFactory.getLogger(EmployeeServiceImpl.class);
	private static final String TAG ="EmployeeServiceImpl :: {} ";

	@Autowired
	private EmployeeRepository employeeRepository;
	

	@Override
	public List<Employee> getAllEmployee() {
		return employeeRepository.findAll();
	}
	
	@Override
	public Employee saveEmployee(Employee obj) {
		return employeeRepository.save(obj);
	}

	@Override
	public Employee updateEmployee(Employee obj) {
		return employeeRepository.save(obj);
	}

	@Override
	public boolean deleteEmployee (Employee obj) {
		try {
			employeeRepository.delete(obj);
			return true;
		}catch(Exception ex) {
			log.error(TAG, ex.getMessage());
			return false;
		}
	}

	
}
