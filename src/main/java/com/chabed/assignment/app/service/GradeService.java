package com.chabed.assignment.app.service;


import java.util.List;

import com.chabed.assignment.app.entity.Grade;


public interface GradeService {
	public List<Grade> getAllGrade();
	public Grade fintById(int id);
	public Grade saveGrade(Grade obj);
	public Grade updateGrade(Grade obj);
	public boolean deleteGrade(Grade obj);
}
