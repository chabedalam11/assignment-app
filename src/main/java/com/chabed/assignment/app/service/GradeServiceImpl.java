package com.chabed.assignment.app.service;



import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chabed.assignment.app.entity.Grade;
import com.chabed.assignment.app.repo.GradeRepository;


@Service("GradeService")
public class GradeServiceImpl implements GradeService{
	private static final Logger log = LoggerFactory.getLogger(GradeServiceImpl.class);
	private static final String TAG ="GradeServiceImpl :: {} ";

	@Autowired
	private GradeRepository gradeRepository;
	
	@Override
	public Grade fintById(int id) {
		return gradeRepository.findById(id);
	}

	@Override
	public List<Grade> getAllGrade() {
		return gradeRepository.findAllByOrderByIdAsc();
	}
	
	@Override
	public Grade saveGrade(Grade obj) {
		return gradeRepository.save(obj);
	}

	@Override
	public Grade updateGrade(Grade obj) {
		return gradeRepository.save(obj);
	}

	@Override
	public boolean deleteGrade (Grade obj) {
		try {
			gradeRepository.delete(obj);
			return true;
		}catch(Exception ex) {
			log.error(TAG, ex.getMessage());
			return false;
		}
	}

	

	
}
