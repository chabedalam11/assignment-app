package com.chabed.assignment.app.service;



import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chabed.assignment.app.entity.ComAccount;
import com.chabed.assignment.app.entity.Transaction;
import com.chabed.assignment.app.repo.TransactionRepository;


@Service("TransactionService")
public class TransactionServiceImpl implements TransactionService{
	private static final Logger log = LoggerFactory.getLogger(TransactionServiceImpl.class);
	private static final String TAG ="TransactionServiceImpl :: {} ";

	@Autowired
	private TransactionRepository transactionRepository;
	

	@Override
	public List<Transaction> getAllTransaction() {
		return transactionRepository.findAll();
	}
	
	@Override
	public List<Transaction> findByComAccount(ComAccount comAccount) {
		return transactionRepository.findByComAccount(comAccount);
	}
	
	@Override
	public Transaction saveTransaction(Transaction obj) {
		return transactionRepository.save(obj);
	}

	@Override
	public Transaction updateTransaction(Transaction obj) {
		return transactionRepository.save(obj);
	}

	@Override
	public boolean deleteTransaction (Transaction obj) {
		try {
			transactionRepository.delete(obj);
			return true;
		}catch(Exception ex) {
			log.error(TAG, ex.getMessage());
			return false;
		}
	}

	

	
}
