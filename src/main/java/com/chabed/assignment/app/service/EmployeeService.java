package com.chabed.assignment.app.service;


import java.util.List;

import com.chabed.assignment.app.entity.Employee;


public interface EmployeeService {
	public List<Employee> getAllEmployee();
	public Employee saveEmployee(Employee obj);
	public Employee updateEmployee(Employee obj);
	public boolean deleteEmployee(Employee obj);
}
