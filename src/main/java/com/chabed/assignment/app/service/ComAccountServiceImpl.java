package com.chabed.assignment.app.service;



import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chabed.assignment.app.entity.ComAccount;
import com.chabed.assignment.app.repo.ComAccountRepository;


@Service("ComAccountService")
public class ComAccountServiceImpl implements ComAccountService{
	private static final Logger log = LoggerFactory.getLogger(ComAccountServiceImpl.class);
	private static final String TAG ="ComAccountServiceImpl :: {} ";

	@Autowired
	private ComAccountRepository comAccountRepository;
	

	@Override
	public List<ComAccount> getAllComAccount() {
		return comAccountRepository.findAll();
	}
	
	@Override
	public ComAccount saveComAccount(ComAccount obj) {
		return comAccountRepository.save(obj);
	}

	@Override
	public ComAccount updateComAccount(ComAccount obj) {
		return comAccountRepository.save(obj);
	}

	@Override
	public boolean deleteComAccount (ComAccount obj) {
		try {
			comAccountRepository.delete(obj);
			return true;
		}catch(Exception ex) {
			log.error(TAG, ex.getMessage());
			return false;
		}
	}

	
}
