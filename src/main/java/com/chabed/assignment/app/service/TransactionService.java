package com.chabed.assignment.app.service;


import java.util.List;

import com.chabed.assignment.app.entity.ComAccount;
import com.chabed.assignment.app.entity.Transaction;


public interface TransactionService {
	public List<Transaction> getAllTransaction();
	public List<Transaction> findByComAccount(ComAccount comAccount);
	public Transaction saveTransaction(Transaction obj);
	public Transaction updateTransaction(Transaction obj);
	public boolean deleteTransaction(Transaction obj);
}
