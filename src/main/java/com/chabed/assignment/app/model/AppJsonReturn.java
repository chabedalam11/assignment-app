package com.chabed.assignment.app.model;

import java.io.Serializable;

public class AppJsonReturn implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private boolean success;
	
	private String message;
	
	private Object data;

	public AppJsonReturn() {
		super();
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
