package com.chabed.assignment.app.model;

import java.io.Serializable;

import com.chabed.assignment.app.entity.ComAccount;
import com.chabed.assignment.app.entity.Transaction;

public class ComAccountModel implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private ComAccount comAccount;
	private Transaction transaction;

	public ComAccountModel() {
		super();
	}

	public ComAccount getComAccount() {
		return comAccount;
	}

	public void setComAccount(ComAccount comAccount) {
		this.comAccount = comAccount;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	

}
