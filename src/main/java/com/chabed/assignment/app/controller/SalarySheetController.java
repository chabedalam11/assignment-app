package com.chabed.assignment.app.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class SalarySheetController {
	private static final Logger log = LoggerFactory.getLogger(SalarySheetController.class);
	private static final String TAG = "SalarySheetController :: {} ";

	@RequestMapping(value = "/admin/salarySheet", method = RequestMethod.GET)
	public ModelAndView callComAccountPage() {
		log.info(TAG, "call comAccount");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("currentPage", "salarySheet");
		modelAndView.setViewName("admin/salarySheet");
		return modelAndView;
	}

	
}
