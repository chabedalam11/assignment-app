package com.chabed.assignment.app.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class PaySalaryController {
	private static final Logger log = LoggerFactory.getLogger(PaySalaryController.class);
	private static final String TAG = "PaySalaryController :: {} ";

	@RequestMapping(value = "/admin/paySalary", method = RequestMethod.GET)
	public ModelAndView callpaySalaryPage() {
		log.info(TAG, "call paySalary");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("currentPage", "paySalary");
		modelAndView.setViewName("admin/paySalary");
		return modelAndView;
	}
	
}
