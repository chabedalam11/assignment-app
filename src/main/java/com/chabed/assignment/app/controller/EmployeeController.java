package com.chabed.assignment.app.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.chabed.assignment.app.service.EmployeeService;


@Controller
public class EmployeeController {
	private static final Logger log = LoggerFactory.getLogger(EmployeeController.class);
	private static final String TAG = "EmployeeController :: {} ";

	@Autowired
	EmployeeService employeeService;
	

	@RequestMapping(value = "/admin/employee", method = RequestMethod.GET)
	public ModelAndView inita001Account() {
		log.info(TAG, "call employee");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("currentPage", "employee");
		modelAndView.setViewName("admin/employee");
		return modelAndView;
	}

	
}
