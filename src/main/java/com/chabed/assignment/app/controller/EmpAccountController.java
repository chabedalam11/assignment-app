package com.chabed.assignment.app.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class EmpAccountController {
	private static final Logger log = LoggerFactory.getLogger(EmpAccountController.class);
	private static final String TAG = "EmpAccountController :: {} ";

	@RequestMapping(value = "/admin/empAccount", method = RequestMethod.GET)
	public ModelAndView inita001Account() {
		log.info(TAG, "call empAccount");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("currentPage", "empAccount");
		modelAndView.setViewName("admin/empAccount");
		return modelAndView;
	}

	
}
