package com.chabed.assignment.app.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class ComAccountController {
	private static final Logger log = LoggerFactory.getLogger(ComAccountController.class);
	private static final String TAG = "ComAccountController :: {} ";

	@RequestMapping(value = "/admin/comAccount", method = RequestMethod.GET)
	public ModelAndView callComAccountPage() {
		log.info(TAG, "call comAccount");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("currentPage", "comAccount");
		modelAndView.setViewName("admin/comAccount");
		return modelAndView;
	}

	
}
