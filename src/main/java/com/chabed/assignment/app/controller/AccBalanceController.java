package com.chabed.assignment.app.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class AccBalanceController {
	private static final Logger log = LoggerFactory.getLogger(AccBalanceController.class);
	private static final String TAG = "AccBalanceController :: {} ";

	@RequestMapping(value = "/admin/accBalance", method = RequestMethod.GET)
	public ModelAndView callComAccountPage() {
		log.info(TAG, "call comAccount");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("currentPage", "accBalance");
		modelAndView.setViewName("admin/accBalance");
		return modelAndView;
	}
}
