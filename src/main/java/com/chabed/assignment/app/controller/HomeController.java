package com.chabed.assignment.app.controller;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.chabed.assignment.app.utils.AppUtils;

@Controller
@RestController
public class HomeController {
	private static final Logger log = LoggerFactory.getLogger(GeneralController.class);
	private static final String TAG = "HomeController :: {} ";
	
	@Autowired
	AppUtils appUtils;
	
	@RequestMapping(value = "/admin/home", method = RequestMethod.GET)
	public ModelAndView home() {
		log.info(TAG, "call home page");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("currentPage", "home");
		modelAndView.setViewName("admin/home");
		return modelAndView;
	}
	
}
