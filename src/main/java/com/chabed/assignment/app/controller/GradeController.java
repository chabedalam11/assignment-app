package com.chabed.assignment.app.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.chabed.assignment.app.service.GradeService;


@Controller
public class GradeController {
	private static final Logger log = LoggerFactory.getLogger(GradeController.class);
	private static final String TAG = "GradeController :: {} ";

	@Autowired
	GradeService gradeService;

	@RequestMapping(value = "/admin/grade", method = RequestMethod.GET)
	public ModelAndView inita001Account() {
		log.info(TAG, "call grade");
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("currentPage", "grade");
		modelAndView.setViewName("admin/grade");
		return modelAndView;
	}


}
