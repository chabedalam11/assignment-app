/************************** << Property >> **************************/
 	var TAG = "Employee: "
	var URL_API_EMP	= '/api/employee';
 	var URL_API_GRADE	= '/api/grade';
	var objList;
	var gradeList;
 	var tableEmployee;
 	
/************************** << GET SERVER INIT DATA >> **************************/
	_loadData();
	_loadDataGrade();
	
    function _loadData() {
    	$.ajax({
		    type : GET, //common js
		    dataType: 'json',
		    contentType:'application/json',
		    url : URL_API_EMP,
		    mimeType: 'application/json',
		    success : function(response) {
		    	objList = response;
            	/*recreate data table*/
            	if(tableEmployee != undefined){
            		tableEmployee.clear().draw();
            		tableEmployee.destroy();
            		tableEmployee= "";
            	}
				LoadInvoiceDetails(objList);
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
     }
    
    function _loadDataGrade() {
		$.ajax({
			type : GET, //common js
			dataType: 'json',
			contentType:'application/json',
			url : URL_API_GRADE,
			mimeType: 'application/json',
			success : function(response) {
				gradeList = response;
				var options;
				jQuery.each(gradeList, function(index, item) {
					options += '<option value="'+item.id+'">'+item.name+'</option>'; //add the option element as a string
				});
				$('#selectEmpGrade').html(options);
				$('#selectEmpGradeU').html(options);
			},
			error : function(e) {
				console.log(e);
				_error(e);
			}
		});
    }

/************************** << CURD >> **************************/
	/*Save data*/
	function saveData(data){
		$.ajax({
		    type : POST, //common js
		    beforeSend: function(){
		    	showLoader(); //common js
		    },
		    dataType: 'json',
		    contentType:'application/json',
		    url : URL_API_EMP,
		    mimeType: 'application/json',
		    data:JSON.stringify(data),
		    success : function(response) {
		    	_success(response);
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
	}
	
	/*update*/
	function updateData(data){
		$.ajax({
		    type : PUT, //common js
		    beforeSend: function(){
		    	showLoader(); //common js
		    },
		    dataType: 'json',
		    contentType:'application/json',
		    url : URL_API_EMP,
		    mimeType: 'application/json',
		    data:JSON.stringify(data),
		    success : function(response) {
		    	_success(response);
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
	}
	
	/*delete*/
	function deleteData(data){
		$.ajax({
		    type : DELETE, //common js
		    beforeSend: function(){
		    	showLoader(); //common js
		    },
		    dataType: 'json',
		    contentType:'application/json',
		    url : URL_API_EMP,
		    mimeType: 'application/json',
		    data:JSON.stringify(data),
		    success : function(response) {
		    	_success(response);
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
	}
	
/************************** << FUNCTION >> **************************/
	$(document).on('click', '#save', function(){ 
		var empId = $( "#txtEmpId" ).val();
		var empName = $( "#txtEmpName" ).val();
		var empGrade = $( "#selectEmpGrade" ).val();
		var empMobile = $( "#txtEmpMobile" ).val();
		var empAddress = $( "#txtEmpAddress" ).val();
		
		if(!isEmpty(empId, "#vEmpId") && !isEmpty(empName, "#vEmpName")){
			if(!isEmpIdValid(empId, "#vEmpId")){
				return;
			}else if(empMobile != "" && !validatePhone(empMobile, "#vEmpMobile")){
				return;
			}
			
			var selectedGrade;
			jQuery.each(gradeList, function(index, item) {
				if(item.id == empGrade){
					selectedGrade = item;
					return;
				}
			});
			
			var alreadySaved = 0;
			jQuery.each(objList, function(index, item) {
				if(item.grade.id == selectedGrade.id){
					alreadySaved += 1;
				}
			});
			
			if(selectedGrade.maxPost <= alreadySaved){
				showErrorAlert('maximum post of '+selectedGrade.name +" already added..");
				return;
			}
			
			
			var data = {
					empId: empId,
					empName: empName,
					grade: {id:empGrade},
					mobile: empMobile,
					address: empAddress,
				}
			
			saveData(data)
		}
			
	});
	
	
	
	$(document).on('click', '#update', function(){ 
		var id = $( "#txtIdU" ).val();
		var empId = $( "#txtEmpIdU" ).val();
		var empName = $( "#txtEmpNameU" ).val();
		var empGrade = $( "#selectEmpGradeU" ).val();
		var empMobile = $( "#txtEmpMobileU" ).val();
		var empAddress = $( "#txtEmpAddressU" ).val();
		
		var data = {
				id : id,
				empId: empId,
				empName: empName,
				grade: {id:empGrade},
				mobile: empMobile,
				address: empAddress,
			}
		
		$('#modelEmployee').modal('hide');
		updateData(data);
	});
	

/************************** << HELPER >> **************************/    
	/*SERVER MSG SECTION*/
    function _success(res, msg) {
        _loadData();
        _clearFormData();
        hideLoader(); //common js
		if(res.success){
			showSuccessAlert(res.message); //common js
		}else{
			showErrorAlert(res.message); //common js
		}
        
    }
 
    function _error(res) {
    	hideLoader(); //common js
        var data = res.data;
        var status = res.status;
        var header = res.header;
        var config = res.config;
        showErrorAlert(status); //common js
    }
    
    function _clearFormData(){
    	$( "#txtEmpId" ).val('');
		$( "#txtEmpName" ).val('');
		$( "#txtEmpMobile" ).val('');
		$( "#txtEmpAddress" ).val('');
    }
    
    function isEmpty(value, showError){
    	if(value == ''){
			$(showError).text("empty not allow !!");
			return true;
		}else {
			$(showError).text("");
			return false;
		}
    }
    
    function isEmpIdValid(empId, showError){
		if(!$.isNumeric(empId)){
			$(showError).text("only number allow !!");
			return false;
		}else if(empId.length != 4){
			$(showError).text("employee id should be 4 digit !!");
			return false;
		}
		
		var alreadyExist = false;
		jQuery.each(objList, function(index, item) {
			if(item.empId == empId){
				alreadyExist = true;
			}
		});
		
		if(alreadyExist){
			$(showError).text("this id already taken !!");
			return false;
		}
		
		return true;
	}
    
    function validatePhone(txtPhone, showError) {
        var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
        if (filter.test(txtPhone)) {
        	$(showError).text("");
            return true;
        } else {
        	$(showError).text("invalid mobile number");
            return false;
        }
    }
    
    
/************************** << PLUG IN >> **************************/        
	function LoadInvoiceDetails(data) {
    	tableEmployee = $('#dataTableEmp').DataTable( {
    			"lengthMenu": [ [ 10, 25, 50, -1], [ 10, 25, 50, "All"] ],
    			"paging"	: true,
    	        "ordering"	: true,
    	        "info"		: true,
    	        "pagingType": "full_numbers",
    	        data,
                "columns": [
                	{  "className":      'edit',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": '' },
                    {  "className":      'delete',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": '' },
                    { "data": "empId" },
                    { "data": "empName" },
                    { "data": "grade.name" },
                    { "data": "mobile" },
                    { "data": "address" },
  				],
                "bDestroy": true,
                "initComplete": function () {
                	$('#dataTableEmp tbody') .off();
                		
                	  $('#dataTableEmp tbody') .on( 'click', 'tr td.edit', function (e) {
                		var tr = $(this).closest('tr');
               	        var row = tableEmployee.row( tr );
               	        var model = row.data();
               	        
               	 	    $( "#txtIdU" ).val(model.id);
               	 	    $( "#txtEmpIdU" ).val(model.empId);
               	 		$( "#txtEmpNameU" ).val(model.empName);
               	 		$("#selectEmpGradeU").val(model.grade.id).change();
               	 		$( "#txtEmpMobileU" ).val(model.mobile);
               	 		$( "#txtEmpAddressU" ).val(model.address);
               	 	        
               	 	    $('#modelEmployee').modal('show');
                	 
                	  });
                	  
                	  $('#dataTableEmp tbody').on( 'click', 'tr td.delete', function (e) {
                		  if (confirm(CONFIRM_DELETE_MSG)) { //common js
               	 	        var tr = $(this).closest('tr');
               	 	        var row = tableEmployee.row( tr );
               	 	        var model = row.data();
               	 	      	deleteData(model)
               	           return true;
               	   	    } 
                	  });
                }
    		}); 
    }