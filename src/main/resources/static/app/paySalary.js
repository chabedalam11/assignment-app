/************************** << Property >> **************************/
 	var TAG = "PaySalary: "
	var URL_API_COM_ACCOUNT	= '/api/comAccount';
 	var URL_API_TRANSACTION	= '/api/transaction';
 	var URL_API_EMP_ACCOUNT	= '/api/empAccount';
 	
	var objList;
	var transactionList;
	var empAccountList;
	var currentBalance = 0;
	var crAmount = 0;
	var drAmount = 0;
	
 	var dataTableContainer;
 	
/************************** << GET SERVER INIT DATA >> **************************/
 	
	_loadData();
	_loadDataEmpAccount();
	
	function _loadData() {
    	$.ajax({
		    type : GET, //common js
		    dataType: 'json',
		    contentType:'application/json',
		    url : URL_API_COM_ACCOUNT,
		    mimeType: 'application/json',
		    success : function(response) {
		    	objList = response;
            	/*recreate data table*/
            	if(dataTableContainer != undefined){
            		dataTableContainer.clear().draw();
            		dataTableContainer.destroy();
            		dataTableContainer= "";
            	}
            	
            	if(objList.length > 0){
            		$('#divForm').hide();
            		$('#divDetails').show();
            		
            		var info = objList[0];
            		$('#lblBank').text(info.bankName+" "+info.branchName);
            		$('#lblAccount').text(info.accName+" ("+info.accNumber+")");
            		_loadDataTransaction(info.id);
            	}else{
            		$('#divForm').show();
            		$('#divDetails').hide();
            	}
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
     }
    
    function _loadDataTransaction(comId) {
    	$.ajax({
		    type : GET, //common js
		    dataType: 'json',
		    contentType:'application/json',
		    url : URL_API_TRANSACTION+"/"+comId,
		    mimeType: 'application/json',
		    success : function(response) {
		    	transactionList = response;
		    	crAmount = 0;
		    	drAmount = 0;
		    	
		    	jQuery.each(transactionList, function(index, item) {
		    		crAmount += item.crAmount;
		    		drAmount += item.drAmount;
				});
		    	currentBalance = crAmount - drAmount;
		    	$('#lbltype').text("Current Balance : "+currentBalance);
		    	
		    	loadDataTable(objList);
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
     }
    
    function _loadDataEmpAccount() {
    	$.ajax({
		    type : GET, //common js
		    dataType: 'json',
		    contentType:'application/json',
		    url : URL_API_EMP_ACCOUNT,
		    mimeType: 'application/json',
		    success : function(response) {
		    	empAccountList = response;
		    	var options;
				jQuery.each(empAccountList, function(index, item) {
					options += '<option value="'+item.id+'">'+"("+item.accNumber+") "+item.accName+'</option>'; //add the option element as a string
				});
				$('#selectEmpAccount').html(options);
				if(empAccountList.length > 0){
					addEmpAccInfo(empAccountList[0]);
				}
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
     }

/************************** << CURD >> **************************/
	/*Save data*/
	function saveTransaction(data){
		$.ajax({
		    type : POST, //common js
		    beforeSend: function(){
		    	showLoader(); //common js
		    },
		    dataType: 'json',
		    contentType:'application/json',
		    url : URL_API_TRANSACTION,
		    mimeType: 'application/json',
		    data:JSON.stringify(data),
		    success : function(response) {
		    	_success(response);
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
	}
	
/************************** << FUNCTION >> **************************/
	$(document).on('click', '#tranAmount', function(){ 
		var amount = $( "#txtTransferAmount" ).val();
		var selectEmpAccount = getSelectedEmpAccount();
		if(!isEmpty(amount, "#vTransferAmount")){
			
			if(!$.isNumeric(amount)){
				$("#vTransferAmount").text("only number allow !!");
				return;
			}
			
			if(amount > currentBalance){
				$("#vTransferAmount").text("insufficient balance !!");
				$("#linkAddAmount").show();
				return;
			}
			
			var data = {
					des: "Transfer to ("+selectEmpAccount.accNumber+") "+selectEmpAccount.accName,
					drAmount: amount,
					comAccount: objList[0],
					empAccount: selectEmpAccount,
				}
			
			saveTransaction(data);
		}
	});
	
	$(document).on('change', '#selectEmpAccount', function(){ 
		var selectEmpAccount = getSelectedEmpAccount();
		 addEmpAccInfo(selectEmpAccount);
    });
	
/************************** << HELPER >> **************************/    
	/*SERVER MSG SECTION*/
    function _success(res, msg) {
        _loadData();
        _clearFormData();
        hideLoader(); //common js
		if(res.success){
			showSuccessAlert(res.message); //common js
		}else{
			showErrorAlert(res.message); //common js
		}
    }
 
    function _error(res) {
    	hideLoader(); //common js
        var data = res.data;
        var status = res.status;
        var header = res.header;
        var config = res.config;
        showErrorAlert(status); //common js
    }
    
    function _clearFormData(){
    	$( "#txtTransferAmount" ).val('');
    }
    
    function isEmpty(value, showError){
    	if(value == ''){
			$(showError).text("empty not allow !!");
			return true;
		}else {
			$(showError).text("");
			return false;
		}
    }
    
    function getSelectedEmpAccount(){
		var selectEmpAccount;
		var empAccountId = $( "#selectEmpAccount" ).val();
		jQuery.each(empAccountList, function(index, item) {
			if(item.id == empAccountId){
				selectEmpAccount = item; 
			}
		});
		return selectEmpAccount;
	}
  
    function addEmpAccInfo (selectEmpAccount ) {
  		var tableData =  '<table cellpadding="5" cellspacing="0" width="100%" border="0" >'+
			  	        '<thead style="font-size: 11px; color: white; background: gray;">'+
			  	        	'<tr >'+
			  		            '<th>Employee</th>'+
			  		            '<th>Bank Account</th>'+
			  		            '<th>Bank Name</th>'+
			  		            '<th>Grade</th>'+
			  		            '<th>Salary</th>'+
			  	            '</tr>'+
			  	        '</thead>'+
			  	        '<tr style="font-size: 11px; color: white; background: lightseagreen;">'+
			  	        	'<td>'+selectEmpAccount.employee.empId+". "+selectEmpAccount.employee.empName+'</td>'+
			  	        	'<td>'+"("+selectEmpAccount.accNumber+") "+selectEmpAccount.accName+'</td>'+
			  	        	'<td>'+selectEmpAccount.bankName+" "+selectEmpAccount.branchName+'</td>'+
			        		'<td>'+selectEmpAccount.employee.grade.name+'</td>'+
			        		'<td>'+selectEmpAccount.employee.grade.salary+'</td>'+
			    		'</tr>'
			  		'</table>';
  		$("#divEmpAccInfo").html(tableData);
  		$( "#txtTransferAmount" ).val(selectEmpAccount.employee.grade.salary);
  	 }
	
/************************** << PLUG IN >> **************************/        
	function loadDataTable(data) {
    	dataTableContainer = $('#dataTableID').DataTable( {
    			"lengthMenu": [ [ 10, 25, 50, -1], [ 10, 25, 50, "All"] ],
    			"paging"	: true,
    	        "ordering"	: true,
    	        "info"		: true,
    	        "pagingType": "full_numbers",
    	        data,
                "columns": [
                	{  "className":      'details-control',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": '' },
                        {
                            "data": "accNumber",
                             render: function(data, type, row, meta)
                               {
                                   return row.accName+' ('+row.accNumber+') '+row.bankName+' '+row.branchName;
                               },
                         },
                        
                    { "defaultContent": drAmount },
                    { "defaultContent": crAmount },
                    { "defaultContent": currentBalance },
  				],
                "bDestroy": true,
                "initComplete": function () {
                  $('#dataTableID tbody') .off();
	        	  /*Add event listener for opening and closing details*/
	      	      $('#dataTableID tbody').off().on('click', 'td.details-control', function () {
	      	        var tr = $(this).closest('tr');
	      	        var row = dataTableContainer.row( tr );
	      	 
	      	        if ( row.child.isShown() ) {
	      	            row.child.hide();
	      	            tr.removeClass('shown');
	      	        }else {
	      	            row.child(showFullInfo()).show();
	      	            tr.addClass('shown');
	      	        }
	      	      });
                }
    		}); 
    	
    	
    	// formate data and show when Open this row in table
   	 function showFullInfo ( ) {
   		 var tableRow='';
   		 jQuery.each( transactionList, function( i, val ) {
   			 	var color = val.crAmount > 0 ? "lightslategray" : "lightcoral";
				 tableRow += '<tr style="font-size: 11px; color: white; background: '+color+';">'+
		            '<td>'+val.des+'</td>'+
		            '<td>'+val.drAmount+'</td>'+
		            '<td>'+val.crAmount+'</td>'+
				'</tr>';
   		 });
   		 
   		return '<table cellpadding="5" cellspacing="0" width="100%" border="0" >'+
   	        '<thead style="font-size: 11px; color: white; background: #226c79;">'+
   	        	'<tr >'+
   		            '<th>Description</th>'+
   		            '<th>Dr. Amount</th>'+
   		            '<th>Cr. Amount </th>'+
   	            '</tr>'+
   	        '</thead>'+
   	        tableRow+
   		'</table>';
   	 } 
    	
    }