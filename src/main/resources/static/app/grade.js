/************************** << Property >> **************************/
 	var TAG = "Grade: "
	var COMMON_API_URL	= '/api/grade';
	var objList;
 	var dataTableContainer;
 	
/************************** << GET SERVER INIT DATA >> **************************/
	_loadData();
    function _loadData() {
    	$.ajax({
		    type : GET, //common js
		    dataType: 'json',
		    contentType:'application/json',
		    url : COMMON_API_URL,
		    mimeType: 'application/json',
		    success : function(response) {
		    	objList = response;
            	/*recreate data table*/
            	if(dataTableContainer != undefined){
            		dataTableContainer.clear().draw();
            		dataTableContainer.destroy();
            		dataTableContainer= "";
            	}
            	if(objList.length > 0){
            		$('#divForm').hide();
            		$( "#txtBasic").prop("disabled", true);
            	}else{
            		$('#divForm').show();
            		$( "#txtBasic").prop("disabled", false);
            	}
				loadDataTable(objList);
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
    	
     }

/************************** << CURD >> **************************/
	/*Save data*/
	function saveData(data){
		$.ajax({
		    type : POST, //common js
		    beforeSend: function(){
		    	showLoader(); //common js
		    },
		    dataType: 'json',
		    contentType:'application/json',
		    url : COMMON_API_URL,
		    mimeType: 'application/json',
		    data:JSON.stringify(data),
		    success : function(response) {
		    	_success(response);
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
	}
	
	/*update*/
	function updateData(data){
		$.ajax({
		    type : PUT, //common js
		    beforeSend: function(){
		    	showLoader(); //common js
		    },
		    dataType: 'json',
		    contentType:'application/json',
		    url : COMMON_API_URL,
		    mimeType: 'application/json',
		    data:JSON.stringify(data),
		    success : function(response) {
		    	_success(response);
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
	}
	
	/*delete*/
	function deleteData(data){
		$.ajax({
		    type : DELETE, //common js
		    beforeSend: function(){
		    	showLoader(); //common js
		    },
		    dataType: 'json',
		    contentType:'application/json',
		    url : COMMON_API_URL,
		    mimeType: 'application/json',
		    data:JSON.stringify(data),
		    success : function(response) {
		    	_success(response);
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
	}
	
/************************** << FUNCTION >> **************************/
	$(document).on('click', '#save', function(){ 
		var basic = $( "#txtBasic" ).val();
		if(isEmpty(basic, "#vSalary")){
			return;
		}else if(!$.isNumeric(basic)){
			$("#vSalary").text("only number allow !!");
			return;
		}
		saveData({ basic: basic })
	});
	
	$(document).on('click', '#update', function(){ 
		var id = $( "#txtIdU" ).val();
		var basic = $( "#txtBasicU" ).val();
		
		var data = {
				id : id,
				basic: basic,
			}
		
		$('#modelUpdate').modal('hide');
		updateData(data);
	});
	

/************************** << HELPER >> **************************/    
	/*SERVER MSG SECTION*/
    function _success(res, msg) {
        _loadData();
        _clearFormData();
        hideLoader(); //common js
		if(res.success){
			showSuccessAlert(res.message); //common js
		}else{
			showErrorAlert(res.message); //common js
		}
        
    }
 
    function _error(res) {
    	hideLoader(); //common js
        var data = res.data;
        var status = res.status;
        var header = res.header;
        var config = res.config;
        showErrorAlert(status); //common js
    }
    
    function _clearFormData(){
		$( "#txtBasic" ).val('');
    }
    
    function isEmpty(value, showError){
    	if(value == ''){
			$(showError).text("empty not allow !!");
			return true;
		}else {
			$(showError).text("");
			return false;
		}
    }
    
    
    
/************************** << PLUG IN >> **************************/        
	function loadDataTable(data) {
    	dataTableContainer = $('#dataTableID').DataTable( {
    			"lengthMenu": [ [ 10, 25, 50, -1], [ 10, 25, 50, "All"] ],
    			"paging"	: true,
    	        "ordering"	: true,
    	        "info"		: true,
    	        "pagingType": "full_numbers",
    	        data,
                "columns": [
                	{  "className":      'edit',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": '' },
                    {  "className":      'delete',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": '' },
                    { "data": "name" },
                    { "data": "maxPost" },
                    { "data": "basic" },
                    { "data": "houseRent" },
                    { "data": "medical" },
                    { "data": "salary" },
  				],
                "bDestroy": true,
                "initComplete": function () {
                	$('#dataTableID tbody') .off();
                		
                	  $('#dataTableID tbody') .on( 'click', 'tr td.edit', function (e) {
                		var tr = $(this).closest('tr');
               	        var row = dataTableContainer.row( tr );
               	        var model = row.data();
               	        
               	 	    $( "#txtIdU" ).val(model.id);
               	 		$( "#txtBasicU" ).val(model.basic);
               	 		
	               	 	if(objList[0].id == model.id){
	                		$( "#txtBasicU").prop("disabled", false);
	                	}else{
	                		$( "#txtBasicU").prop("disabled", true);
	                	}
               	 		
               	 	    $('#modelUpdate').modal('show');
                	  });
                	  
                	  $('#dataTableID tbody').on( 'click', 'tr td.delete', function (e) {
                		  if (confirm(CONFIRM_DELETE_MSG)) { //common js
               	 	        var tr = $(this).closest('tr');
               	 	        var row = dataTableContainer.row( tr );
               	 	        var model = row.data();
               	 	      	deleteData(model)
               	           return true;
               	   	    } 
                	  });
                }
    		}); 
    	
 	    
    }