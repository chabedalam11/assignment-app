/************************** << Property >> **************************/
 	var TAG = "SalarySheet: "
	var URL_API_EMP	= '/api/employee';
	var objList;
 	var tableEmployee;
 	
/************************** << GET SERVER INIT DATA >> **************************/
	_loadData();

	function _loadData() {
    	$.ajax({
		    type : GET, //common js
		    dataType: 'json',
		    contentType:'application/json',
		    url : URL_API_EMP,
		    mimeType: 'application/json',
		    success : function(response) {
		    	objList = response;
            	/*recreate data table*/
            	if(tableEmployee != undefined){
            		tableEmployee.clear().draw();
            		tableEmployee.destroy();
            		tableEmployee= "";
            	}
				LoadInvoiceDetails(objList);
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
     }

/************************** << CURD >> **************************/
	
	
/************************** << FUNCTION >> **************************/
	
	

/************************** << HELPER >> **************************/    
	
    
    
/************************** << PLUG IN >> **************************/        
	function LoadInvoiceDetails(data) {
    	tableEmployee = $('#dataTableEmp').DataTable( {
    			"lengthMenu": [ [ 10, 25, 50, -1], [ 10, 25, 50, "All"] ],
    			"paging"	: true,
    	        "ordering"	: true,
    	        "info"		: true,
    	        "pagingType": "full_numbers",
    	        data,
                "columns": [
                    { "defaultContent": "" },
                    { "data": "empId" },
                    { "data": "empName" },
                    { "data": "mobile" },
                    { "data": "address" },
                    { "data": "grade.name" },
                    { "data": "grade.basic" },
                    { "data": "grade.houseRent" },
                    { "data": "grade.medical" },
                    { "data": "grade.salary" },
  				],
                "bDestroy": true,
                "fnRowCallback" : function(nRow, aData, iDisplayIndex){
                    $("td:first", nRow).html(iDisplayIndex +1);
                   return nRow;
                },
                "footerCallback": function ( row, data, start, end, display ) {
        	        var api = this.api(), data;
        	        // Remove the formatting to get integer data for summation
        	        var intVal = function ( i ) {
        	            return typeof i === 'string' ?
        	                i.replace(/[\$,]/g, '')*1 :
        	                typeof i === 'number' ?
        	                    i : 0;
        	        };
        	        /* ================= 1 ================= */
        	        // Total over all pages
        	        total = api
        	            .column( 9 )
        	            .data()
        	            .reduce( function (a, b) {
        	                return intVal(a) + intVal(b);
        	            }, 0 );

        	        // Total over this page
        	        pageTotal = api
        	            .column( 9, { page: 'current'} )
        	            .data()
        	            .reduce( function (a, b) {
        	                return intVal(a) + intVal(b);
        	            }, 0 );

        	        // Update footer
        	        $( api.column( 9 ).footer() ).html(
        	        	total
        	        );
        	    }
    		}); 
    }