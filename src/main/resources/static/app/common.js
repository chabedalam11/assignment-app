
/*ajax method*/
var GET = 'GET';
var POST = 'POST';
var PUT = 'PUT';
var DELETE = 'DELETE';

/*message*/
var CONFIRM_MSG = "are you sure ?";
var CONFIRM_DELETE_MSG = "do you want to  delete?";
var CONFIRM_RESEND_MAIL_MSG = "do you want to  resend mail?";

var SELECT_TEXT = "Select One";


/*helper*/
function isNumeric(value) {
	return /^\d+$/.test(value);
}

function isDataAlreadyExist(objList, obj){
	var count = 0;
	angular.forEach(objList, function(value, key) {
		if(obj.id != value.id  && obj.name.toLowerCase() == value.name.toLowerCase()){
			count++;
		}
	});
	if(count != 0){
		showErrorAlert(obj.name+' already exist');
		return true;
	}else{
		return false;
	}
}


/*loader*/
function showLoader(){
	$('.ajax-loader').css("visibility", "visible");
}

function hideLoader(){
	$('.ajax-loader').css("visibility", "hidden");
}

/*alert*/
function showSuccessAlert(msg) {
	var dom = '<div class="top-alert"><div class="alert alert-success alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Success!</strong> ' + msg + ' </div></div>';
	var jdom = $(dom);
	jdom.hide();
	$("body").append(jdom);
	jdom.fadeIn();
	setTimeout(function() {
		jdom.fadeOut(function() {
			jdom.remove();
		});
	}, 2000);
}

function showErrorAlert(msg) {
	var dom = '<div class="top-alert"><div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error!</strong> ' + msg + ' </div></div>';
	var jdom = $(dom);
	jdom.hide();
	$("body").append(jdom);
	jdom.fadeIn();
	setTimeout(function() {
		jdom.fadeOut(function() {
			jdom.remove();
		});
	}, 2000);
}