/************************** << Property >> **************************/
 	var TAG = "EmpAccount: "
	var URL_API_EMP_ACCOUNT	= '/api/empAccount';
 	var URL_API_EMP	= '/api/employee';
	var objList;
 	var dataTableContainer;
 	
/************************** << GET SERVER INIT DATA >> **************************/
	_loadData();
	_loadDataEmp();
	
    function _loadData() {
    	$.ajax({
		    type : GET, //common js
		    dataType: 'json',
		    contentType:'application/json',
		    url : URL_API_EMP_ACCOUNT,
		    mimeType: 'application/json',
		    success : function(response) {
		    	objList = response;
            	/*recreate data table*/
            	if(dataTableContainer != undefined){
            		dataTableContainer.clear().draw();
            		dataTableContainer.destroy();
            		dataTableContainer= "";
            	}
				LoadInvoiceDetails(objList);
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
     }
    
    function _loadDataEmp() {
		$.ajax({
			type : GET, //common js
			dataType: 'json',
			contentType:'application/json',
			url : URL_API_EMP,
			mimeType: 'application/json',
			success : function(response) {
				var options;
				jQuery.each(response, function(index, item) {
					options += '<option value="'+item.id+'">'+item.empName+'</option>'; //add the option element as a string
				});
				$('#selectEmp').html(options);
				$('#selectEmpU').html(options);
			},
			error : function(e) {
				console.log(e);
				_error(e);
			}
		});
    }

/************************** << CURD >> **************************/
	/*Save data*/
	function saveData(data){
		$.ajax({
		    type : POST, //common js
		    beforeSend: function(){
		    	showLoader(); //common js
		    },
		    dataType: 'json',
		    contentType:'application/json',
		    url : URL_API_EMP_ACCOUNT,
		    mimeType: 'application/json',
		    data:JSON.stringify(data),
		    success : function(response) {
		    	_success(response);
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
	}
	
	/*update*/
	function updateData(data){
		$.ajax({
		    type : PUT, //common js
		    beforeSend: function(){
		    	showLoader(); //common js
		    },
		    dataType: 'json',
		    contentType:'application/json',
		    url : URL_API_EMP_ACCOUNT,
		    mimeType: 'application/json',
		    data:JSON.stringify(data),
		    success : function(response) {
		    	_success(response);
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
	}
	
	/*delete*/
	function deleteData(data){
		$.ajax({
		    type : DELETE, //common js
		    beforeSend: function(){
		    	showLoader(); //common js
		    },
		    dataType: 'json',
		    contentType:'application/json',
		    url : URL_API_EMP_ACCOUNT,
		    mimeType: 'application/json',
		    data:JSON.stringify(data),
		    success : function(response) {
		    	_success(response);
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
	}
	
/************************** << FUNCTION >> **************************/
	$(document).on('click', '#save', function(){ 
		var empId = $( "#selectEmp" ).val();
		var type = $( "#selectType" ).val();
		var accName = $( "#txtAccName" ).val();
		var accNumber = $( "#txtEmpNumber" ).val();
		var bankName = $( "#txtBankName" ).val();
		var branchName = $( "#txtBranchName" ).val();
	
		if(!isEmpty(accName, "#vAccName") && !isEmpty(accNumber, "#vAccNumber")){
			
			var isExist = false;
			jQuery.each(objList, function(index, item) {
				if(empId == item.employee.id){
					isExist = true;
					return;
				}
			});
			
			if(isExist){
				showErrorAlert('this employee account already added...');
				return;
			}
			
			var data = {
					employee: {id:empId},
					type: type,
					accName: accName,
					accNumber: accNumber,
					bankName: bankName,
					branchName: branchName,
				}
			
			saveData(data)
		}
	});
	
	$(document).on('click', '#update', function(){ 
		var id = $( "#txtIdU" ).val();
		var empId = $( "#selectEmpU" ).val();
		var type = $( "#selectTypeU" ).val();
		var accName = $( "#txtAccNameU" ).val();
		var accNumber = $( "#txtEmpNumberU" ).val();
		var bankName = $( "#txtBankNameU" ).val();
		var branchName = $( "#txtBranchNameU" ).val();
		
		var data = {
				id : id,
				employee: {id:empId},
				type: type,
				accName: accName,
				accNumber: accNumber,
				bankName: bankName,
				branchName: branchName,
			}
		
		$('#modelEmployee').modal('hide');
		updateData(data);
	});
	

/************************** << HELPER >> **************************/    
	/*SERVER MSG SECTION*/
    function _success(res, msg) {
        _loadData();
        _clearFormData();
        hideLoader(); //common js
		if(res.success){
			showSuccessAlert(res.message); //common js
		}else{
			showErrorAlert(res.message); //common js
		}
        
    }
 
    function _error(res) {
    	hideLoader(); //common js
        var data = res.data;
        var status = res.status;
        var header = res.header;
        var config = res.config;
        showErrorAlert(status); //common js
    }
    
    function _clearFormData(){
    	$( "#txtAccName" ).val('');
		$( "#txtEmpNumber" ).val('');
		$( "#txtBankName" ).val('');
		$( "#txtBranchName" ).val('');
    }
    
    function isEmpty(value, showError){
    	if(value == ''){
			$(showError).text("empty not allow !!");
			return true;
		}else {
			$(showError).text("");
			return false;
		}
    }
  
   
    
    
/************************** << PLUG IN >> **************************/        
	function LoadInvoiceDetails(data) {
    	dataTableContainer = $('#dataTableEmp').DataTable( {
    			"lengthMenu": [ [ 10, 25, 50, -1], [ 10, 25, 50, "All"] ],
    			"paging"	: true,
    	        "ordering"	: true,
    	        "info"		: true,
    	        "pagingType": "full_numbers",
    	        data,
                "columns": [
                	{  "className":      'edit',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": '' },
                    {  "className":      'delete',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": '' },
                    { "data": "employee.empName" },
                    { "data": "type" },
                    { "data": "accName" },
                    { "data": "accNumber" },
                    { "data": "bankName" },
                    { "data": "branchName" },
  				],
                "bDestroy": true,
                "initComplete": function () {
                	$('#dataTableEmp tbody') .off();
                		
                	  $('#dataTableEmp tbody') .on( 'click', 'tr td.edit', function (e) {
                		var tr = $(this).closest('tr');
               	        var row = dataTableContainer.row( tr );
               	        var model = row.data();
               	        
               	        $( "#txtIdU" ).val(model.id);
               	        $("#selectEmpU").val(model.employee.id).change();
               	        $("#selectTypeU").val(model.type).change();
               	 	    $( "#txtAccNameU" ).val(model.accName);
               	 	    $( "#txtEmpNumberU" ).val(model.accNumber);
               	 		$( "#txtBankNameU" ).val(model.bankName);
               	 		$( "#txtBranchNameU" ).val(model.branchName);
               	 	        
               	 	    $('#modelEmployee').modal('show');
               	 	    
                	  });
                	  
                	  $('#dataTableEmp tbody').on( 'click', 'tr td.delete', function (e) {
                		  if (confirm(CONFIRM_DELETE_MSG)) { //common js
               	 	        var tr = $(this).closest('tr');
               	 	        var row = dataTableContainer.row( tr );
               	 	        var model = row.data();
               	 	      	deleteData(model)
               	           return true;
               	   	    } 
                	  });
                }
    		}); 
    }