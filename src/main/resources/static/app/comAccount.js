/************************** << Property >> **************************/
 	var TAG = "ComAccount: "
	var URL_API_COM_ACCOUNT	= '/api/comAccount';
 	var URL_API_TRANSACTION	= '/api/transaction';
	var objList;
	var transactionList;
 	var dataTableContainer;
 	
/************************** << GET SERVER INIT DATA >> **************************/
 	
	_loadData();
	function _loadData() {
    	$.ajax({
		    type : GET, //common js
		    dataType: 'json',
		    contentType:'application/json',
		    url : URL_API_COM_ACCOUNT,
		    mimeType: 'application/json',
		    success : function(response) {
		    	objList = response;
            	/*recreate data table*/
            	if(dataTableContainer != undefined){
            		dataTableContainer.clear().draw();
            		dataTableContainer.destroy();
            		dataTableContainer= "";
            	}
            	
            	if(objList.length > 0){
            		$('#divForm').hide();
            		$('#divDetails').show();
            		
            		var info = objList[0];
            		$('#lblBank').text(info.bankName+" "+info.branchName);
            		$('#lblAccount').text(info.accName+" ("+info.accNumber+")");
            		_loadDataTransaction(info.id);
            	}else{
            		$('#divForm').show();
            		$('#divDetails').hide();
            	}
            	
				loadDataTable(objList);
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
     }
    
    function _loadDataTransaction(comId) {
    	$.ajax({
		    type : GET, //common js
		    dataType: 'json',
		    contentType:'application/json',
		    url : URL_API_TRANSACTION+"/"+comId,
		    mimeType: 'application/json',
		    success : function(response) {
		    	transactionList = response;
		    	var crAmount = 0;
		    	var drAmount = 0;
		    	
		    	jQuery.each(transactionList, function(index, item) {
		    		crAmount += item.crAmount;
		    		drAmount += item.drAmount;
				});
		    	var currentBalance = crAmount - drAmount;
		    	$('#lbltype').text("Current Balance : "+currentBalance);
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
     }

/************************** << CURD >> **************************/
	/*Save data*/
	function saveData(data){
		$.ajax({
		    type : POST, //common js
		    beforeSend: function(){
		    	showLoader(); //common js
		    },
		    dataType: 'json',
		    contentType:'application/json',
		    url : URL_API_COM_ACCOUNT,
		    mimeType: 'application/json',
		    data:JSON.stringify(data),
		    success : function(response) {
		    	_success(response);
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
	}
	
	function saveTransaction(data){
		$.ajax({
		    type : POST, //common js
		    beforeSend: function(){
		    	showLoader(); //common js
		    },
		    dataType: 'json',
		    contentType:'application/json',
		    url : URL_API_TRANSACTION,
		    mimeType: 'application/json',
		    data:JSON.stringify(data),
		    success : function(response) {
		    	_success(response);
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
	}
	
	/*update*/
	function updateData(data){
		$.ajax({
		    type : PUT, //common js
		    beforeSend: function(){
		    	showLoader(); //common js
		    },
		    dataType: 'json',
		    contentType:'application/json',
		    url : URL_API_COM_ACCOUNT,
		    mimeType: 'application/json',
		    data:JSON.stringify(data),
		    success : function(response) {
		    	_success(response);
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
	}
	
	/*delete*/
	function deleteData(data){
		$.ajax({
		    type : DELETE, //common js
		    beforeSend: function(){
		    	showLoader(); //common js
		    },
		    dataType: 'json',
		    contentType:'application/json',
		    url : URL_API_COM_ACCOUNT,
		    mimeType: 'application/json',
		    data:JSON.stringify(data),
		    success : function(response) {
		    	_success(response);
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
	}
	
/************************** << FUNCTION >> **************************/
	$(document).on('click', '#save', function(){ 
		var type = $( "#selectType" ).val();
		var accName = $( "#txtAccName" ).val();
		var accNumber = $( "#txtEmpNumber" ).val();
		var bankName = $( "#txtBankName" ).val();
		var branchName = $( "#txtBranchName" ).val();
		var initBalance = $( "#txtInitBalance" ).val();
	
		if(!isEmpty(accName, "#vAccName") && !isEmpty(accNumber, "#vAccNumber") && !isEmpty(initBalance, "#vInitBalance")){
			
			if(!$.isNumeric(initBalance)){
				$("#vInitBalance").text("only number allow !!");
				return;
			}
			
			/*create com account*/
			var comAccount = {
					type: type,
					accName: accName,
					accNumber: accNumber,
					bankName: bankName,
					branchName: branchName,
				}
			
			/*create transaction*/
			var transaction = {
					des: "Initial balance amount",
					crAmount: initBalance,
				}
			
			/*create com account model*/
			var data = {
					comAccount: comAccount,
					transaction: transaction,
				}
			
			saveData(data);
		}
			
	});
	
	$(document).on('click', '#update', function(){ 
		var id = $( "#txtIdU" ).val();
		var type = $( "#selectTypeU" ).val();
		var accName = $( "#txtAccNameU" ).val();
		var accNumber = $( "#txtEmpNumberU" ).val();
		var bankName = $( "#txtBankNameU" ).val();
		var branchName = $( "#txtBranchNameU" ).val();
		
		var data = {
				id : id,
				type: type,
				accName: accName,
				accNumber: accNumber,
				bankName: bankName,
				branchName: branchName,
			}
		
		$('#modelEmployee').modal('hide');
		updateData(data);
	});
	
	$(document).on('click', '#addAmount', function(){ 
		var amount = $( "#txtAddAmount" ).val();
	
		if(!isEmpty(amount, "#vAddAmount")){
			
			if(!$.isNumeric(amount)){
				$("#vAddAmount").text("only number allow !!");
				return;
			}
			
			var data = {
					des: "Add amount",
					crAmount: amount,
					comAccount: objList[0],
				}
			
			saveTransaction(data);
		}
			
	});

/************************** << HELPER >> **************************/    
	/*SERVER MSG SECTION*/
    function _success(res, msg) {
        _loadData();
        _clearFormData();
        hideLoader(); //common js
		if(res.success){
			showSuccessAlert(res.message); //common js
		}else{
			showErrorAlert(res.message); //common js
		}
        
    }
 
    function _error(res) {
    	hideLoader(); //common js
        var data = res.data;
        var status = res.status;
        var header = res.header;
        var config = res.config;
        showErrorAlert(status); //common js
    }
    
    function _clearFormData(){
    	$( "#txtAccName" ).val('');
		$( "#txtEmpNumber" ).val('');
		$( "#txtBankName" ).val('');
		$( "#txtBranchName" ).val('');
		$( "#txtAddAmount" ).val('');
    }
    
    function isEmpty(value, showError){
    	if(value == ''){
			$(showError).text("empty not allow !!");
			return true;
		}else {
			$(showError).text("");
			return false;
		}
    }
  
    
    
    
/************************** << PLUG IN >> **************************/        
	function loadDataTable(data) {
    	dataTableContainer = $('#dataTableID').DataTable( {
    			"lengthMenu": [ [ 10, 25, 50, -1], [ 10, 25, 50, "All"] ],
    			"paging"	: true,
    	        "ordering"	: true,
    	        "info"		: true,
    	        "pagingType": "full_numbers",
    	        data,
                "columns": [
                	{  "className":      'details-control',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": '' },
                    {  "className":      'edit',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": '' },
                    {  "className":      'delete',
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": '' },
                    { "data": "type" },
                    { "data": "accName" },
                    { "data": "accNumber" },
                    { "data": "bankName" },
                    { "data": "branchName" },
  				],
                "bDestroy": true,
                "initComplete": function () {
                	$('#dataTableID tbody') .off();
                		
                	  $('#dataTableID tbody') .on( 'click', 'tr td.edit', function (e) {
                		var tr = $(this).closest('tr');
               	        var row = dataTableContainer.row( tr );
               	        var model = row.data();
               	        
               	        $( "#txtIdU" ).val(model.id);
               	        $("#selectTypeU").val(model.type).change();
               	 	    $( "#txtAccNameU" ).val(model.accName);
               	 	    $( "#txtEmpNumberU" ).val(model.accNumber);
               	 		$( "#txtBankNameU" ).val(model.bankName);
               	 		$( "#txtBranchNameU" ).val(model.branchName);
               	 	        
               	 	    $('#modelEmployee').modal('show');
               	 	    
                	  });
                	  
                	  $('#dataTableID tbody').on( 'click', 'tr td.delete', function (e) {
                		  if (confirm(CONFIRM_DELETE_MSG)) { //common js
               	 	        var tr = $(this).closest('tr');
               	 	        var row = dataTableContainer.row( tr );
               	 	        var model = row.data();
               	 	      	deleteData(model)
               	           return true;
               	   	    } 
                	  });
                	  
                	  /*Add event listener for opening and closing details*/
              	      $('#dataTableID tbody').off().on('click', 'td.details-control', function () {
              	        var tr = $(this).closest('tr');
              	        var row = dataTableContainer.row( tr );
              	 
              	        if ( row.child.isShown() ) {
              	            row.child.hide();
              	            tr.removeClass('shown');
              	        }else {
              	            row.child(showFullInfo()).show();
              	            tr.addClass('shown');
              	        }
              	      });
                }
    		}); 
    	
    	
    	// formate data and show when Open this row in table
   	 function showFullInfo ( ) {
   		 var tableRow='';
   		 jQuery.each( transactionList, function( i, val ) {
				 tableRow += '<tr style="font-size: 11px; color: white; background: #31919b;">'+
		            '<td>'+val.des+'</td>'+
		            '<td>'+val.drAmount+'</td>'+
		            '<td>'+val.crAmount+'</td>'+
				'</tr>';
   		 });
   		 
   		return '<table cellpadding="5" cellspacing="0" width="100%" border="0" >'+
   	        '<thead style="font-size: 11px; color: white; background: #226c79;">'+
   	        	'<tr >'+
   		            '<th>Description</th>'+
   		            '<th>Dr. Amount</th>'+
   		            '<th>Cr. Amount </th>'+
   	            '</tr>'+
   	        '</thead>'+
   	        tableRow+
   		'</table>';
   	 } 
    	
    }