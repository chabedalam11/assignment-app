/************************** << Property >> **************************/
 	var TAG = "AccBalance: "
	var URL_API_COM_ACCOUNT	= '/api/comAccount';
 	var URL_API_TRANSACTION	= '/api/transaction';
 	
	var objList;
	var transactionList;
	var currentBalance = 0;
	var crAmount = 0;
	var drAmount = 0;
	
 	var dataTableContainer;
 	
/************************** << GET SERVER INIT DATA >> **************************/
 	
	_loadData();
	
	function _loadData() {
    	$.ajax({
		    type : GET, //common js
		    dataType: 'json',
		    contentType:'application/json',
		    url : URL_API_COM_ACCOUNT,
		    mimeType: 'application/json',
		    success : function(response) {
		    	objList = response;
            	/*recreate data table*/
            	if(dataTableContainer != undefined){
            		dataTableContainer.clear().draw();
            		dataTableContainer.destroy();
            		dataTableContainer= "";
            	}
            	
            	if(objList.length > 0){
            		$('#divForm').hide();
            		$('#divDetails').show();
            		
            		var info = objList[0];
            		$('#lblBank').text(info.bankName+" "+info.branchName);
            		$('#lblAccount').text(info.accName+" ("+info.accNumber+")");
            		_loadDataTransaction(info.id);
            	}else{
            		$('#divForm').show();
            		$('#divDetails').hide();
            	}
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
     }
    
    function _loadDataTransaction(comId) {
    	$.ajax({
		    type : GET, //common js
		    dataType: 'json',
		    contentType:'application/json',
		    url : URL_API_TRANSACTION+"/"+comId,
		    mimeType: 'application/json',
		    success : function(response) {
		    	transactionList = response;
		    	crAmount = 0;
		    	drAmount = 0;
		    	var drList = [];
		    	jQuery.each(transactionList, function(index, item) {
		    		crAmount += item.crAmount;
		    		drAmount += item.drAmount;
		    		if(item.drAmount > 0){
		    			drList.push(item);
		    		}
				});
		    	currentBalance = crAmount - drAmount;
		    	$('#lblPaidSalary').text("Total Paid Salary : "+drAmount);
		    	$('#lblRemainBalance').text("Remaining Balance : "+currentBalance);
		    	
		    	loadDataTable(drList);
		    },
		    error : function(e) {
		       console.log(e);
		       _error(e);
		    }
		});
     }
    
/************************** << CURD >> **************************/
	
/************************** << FUNCTION >> **************************/
	
/************************** << HELPER >> **************************/    
    
/************************** << PLUG IN >> **************************/        
	function loadDataTable(data) {
    	dataTableContainer = $('#dataTableID').DataTable( {
			"lengthMenu": [ [ 10, 25, 50, -1], [ 10, 25, 50, "All"] ],
			"paging"	: true,
	        "ordering"	: true,
	        "info"		: true,
	        "pagingType": "full_numbers",
	        data,
            "columns": [
            	{ "defaultContent": "" },
                {
                   "data": "des",
                   render: function(data, type, row, meta)
                   {
        	   		return row.empAccount.employee.empId+'. '+row.empAccount.employee.empName+' ('+row.empAccount.employee.grade.name+")";
                   },
                },
                { "data": "des" },
                { "data": "drAmount" },
			],
            "bDestroy": true,
            "fnRowCallback" : function(nRow, aData, iDisplayIndex){
                $("td:first", nRow).html(iDisplayIndex +1);
               return nRow;
            },
            "footerCallback": function ( row, data, start, end, display ) {
    	        var api = this.api(), data;
    	        // Remove the formatting to get integer data for summation
    	        var intVal = function ( i ) {
    	            return typeof i === 'string' ?
    	                i.replace(/[\$,]/g, '')*1 :
    	                typeof i === 'number' ?
    	                    i : 0;
    	        };
    	        /* ================= 1 ================= */
    	        // Total over all pages
    	        total = api
    	            .column( 3 )
    	            .data()
    	            .reduce( function (a, b) {
    	                return intVal(a) + intVal(b);
    	            }, 0 );

    	        // Total over this page
    	        pageTotal = api
    	            .column( 3, { page: 'current'} )
    	            .data()
    	            .reduce( function (a, b) {
    	                return intVal(a) + intVal(b);
    	            }, 0 );

    	        // Update footer
    	        $( api.column( 3 ).footer() ).html(
    	        	total
    	        );
    	    }
		}); 
    }